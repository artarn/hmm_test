module.exports = {
    rootUrl: 'http://hm.dev/',
    gridUrl: 'http://localhost:4444/wd/hub',

    browsers: {
        chrome: {
            desiredCapabilities: {
                browserName: 'chrome',
                setWindowSize: '3000x2000'
            }
        }
    }
};